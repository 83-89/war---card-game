program warCardGameProject;

uses
  Vcl.Forms,
  warCardGameUnit in 'warCardGameUnit.pas' {warForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TwarForm, warForm);
  Application.Run;
end.
