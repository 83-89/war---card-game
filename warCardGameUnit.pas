unit warCardGameUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Generics.Collections, System.Generics.Defaults,
  Vcl.ExtCtrls, PNGImage, Vcl.ComCtrls;

type
  TIntegerComparer = TVarCompareResult;

type
  TCard =class(TObject)
    private
      FName: string;
      FValue: Integer;
      FOriginalIndex : Integer;
      FRandomIndex: Integer;
      FPicFile: string;
      FShortName : string;

    public
      property Name : string read FName write FName;
      property Value : integer read FValue write FValue;
      property OriginalIndex : integer read FOriginalIndex write FOriginalIndex;
      property RandomIndex : integer read FRandomIndex write FRandomIndex;
      property PicFile : string read FPicFile write FPicFile;
      property ShortName : string read FShortName write FShortName;

    public
      constructor Create; overload;
      constructor Create(_Name : string; _Value,_OriginalIndex, _RandomIndex:Integer;_FPicFile:string;_FShortName:string); overload;

  end;

type
  TPlayer=class(TObject)
    private
      FName : string;
      FPlayerDeck : TList<TCard>;
      FScore : integer;
      FTotalScore : integer;
    public
      property Name : string read FName write FName;
      property PlayerDeck : TList<TCard> read FPlayerDeck write FPlayerDeck;
      property Score : integer read FScore write FScore;
      property TotalScore : integer read FTotalScore write FTotalScore;

    public
      constructor Create; overload;
      constructor Create(_Name : string; _PlayerDeck:TList<TCard>; _Score : integer; _TotalScore : integer); overload;

  end;

type
  TGame=class(TObject)
    private
      FCounter : Integer;
      FPlayer1 : TPlayer;
      FPlayer2 : TPlayer;
      FGameDeck : TList<TCard>;
      FPoolDeck : TList<TCard>;
    public
      property Counter : integer read FCounter write FCounter;
      property Player1 : TPlayer read FPlayer1 write FPlayer1;
      property Player2 : TPlayer read FPlayer2 write FPlayer2;
      property GameDeck : TList<TCard> read FGameDeck write FGameDeck;
      property PoolDeck : TList<TCard> read FPoolDeck write FPoolDeck;

    public
      constructor Create; overload;
      constructor Create(_Counter : integer; _Player1,_Player2:TPlayer;_GameDeck,_PoolDeck: TList<TCard>); overload;

  end;


type
  TwarForm = class(TForm)
    closeButton: TLabel;
    player1Label: TLabel;
    player2Label: TLabel;
    lbl1Round: TLabel;
    lblNumberP1: TLabel;
    lblNumberP2: TLabel;
    lblPlayXRounds: TLabel;
    player1CurrentCard: TImage;
    player2CurrentCard: TImage;
    lblcounter: TLabel;
    lblToTheEnd: TLabel;
    roundLabel: TLabel;
    scoreP1Title: TLabel;
    scoreP2Title: TLabel;
    scoreP1: TLabel;
    scoreP2: TLabel;
    newButton: TLabel;
    helpButton: TLabel;
    rulesPanel: TPanel;
    closeRulesButton: TLabel;
    rulesTitle: TLabel;
    rulesLabel: TLabel;
    lblWinner: TLabel;
    lst1: TListBox;
    lst2: TListBox;
    Player1TotalScoreLabel: TLabel;
    Player2TotalScoreLabel: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    procedure closeButtonClick(Sender: TObject);
    procedure closeButtonLeave(Sender: TObject);
    procedure closeButtonHover(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lbl1RoundClick(Sender: TObject);
    procedure lblPlayXRoundsClick(Sender: TObject);
    procedure lblPlayXRoundsHover(Sender: TObject);
    procedure lblPlayXRoundsLeave(Sender: TObject);
    procedure lbl1RoundHover(Sender: TObject);
    procedure lbl1RoundLeave(Sender: TObject);
    procedure lblToTheEndClick(Sender: TObject);
    procedure lblToTheEndHover(Sender: TObject);
    procedure lblToTheEndLeave(Sender: TObject);
    procedure newButtonClick(Sender: TObject);
    procedure newButtonHover(Sender: TObject);
    procedure newButtonLeave(Sender: TObject);
    procedure helpButtonHover(Sender: TObject);
    procedure helpButtonLeave(Sender: TObject);
    procedure closeRulesHover(Sender: TObject);
    procedure closeRulesLeave(Sender: TObject);
    procedure closeRulesClick(Sender: TObject);
    procedure helpButtonClick(Sender: TObject);
    procedure lblNumberP2Hover(Sender: TObject);
    procedure lblNumberP1Leave(Sender: TObject);
    procedure lblNumberP2Leave(Sender: TObject);
    procedure lblNumberP1Hover(Sender: TObject);
    procedure lblNumberP1Click(Sender: TObject);
    procedure lblNumberP2Click(Sender: TObject);
    procedure Label1Click(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;


var
  warForm: TwarForm;

implementation

{$R *.dfm}

var
  Suits:array[1..4] of string = ('Clubs','Diamonds','Hearts','Spades');
  CardsValue:array[1..13] of string =('Ace','Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten','Jack','Queen','King');
  deck,player1Deck,player2Deck,pool:TList<TCard>;
  Player1,Player2:TPlayer;
  Game:TGame;
  Counter : Integer;


constructor TCard.Create;
begin
  FName:= '';
  FValue:= 0;
  FOriginalIndex := 0;
  FRandomIndex:= 0;
  FPicFile:= '';
  FShortName:='';
end;

constructor TCard.Create(_Name : string; _Value,_OriginalIndex, _RandomIndex:Integer;_FPicFile:string;_FShortName:string);
begin
  FName:= _Name;
  FValue:= _Value;
  FOriginalIndex := _OriginalIndex;
  FRandomIndex:= _RandomIndex;
  FPicFile:= _FPicFile;
  FShortName:= _FShortName;
end;

constructor TPlayer.Create;
begin
  FName:= '';
  FPlayerDeck:=NIL;
  FScore:= 0;
end;

constructor TPlayer.Create(_Name : string; _PlayerDeck:TList<TCard>; _Score : integer; _TotalScore : integer);
begin
  FName:= _Name;
  FPlayerDeck:=_PlayerDeck;
  FScore:= 0;
  FTotalScore:= 0;
end;

constructor TGame.Create;
begin
  FCounter:= 0;
  FPlayer1:=NIL;
  FPlayer2:=NIL;
  FGameDeck:=NIL;
  FPoolDeck:=NIL;
end;

constructor TGame.Create(_Counter : integer; _Player1,_Player2:TPlayer; _GameDeck,_PoolDeck:TList<TCard>);
begin
  FCounter:= _Counter;
  FPlayer1:=_Player1;
  FPlayer2:=_Player2;
  FGameDeck:=_GameDeck;
  FPoolDeck:=_PoolDeck;
end;

procedure TwarForm.closeButtonHover(Sender: TObject);
begin
  closeButton.Font.Style:= [fsBold];
end;

procedure TwarForm.closeButtonLeave(Sender: TObject);
begin
  closeButton.Font.Style:= [];
end;

procedure TwarForm.closeRulesClick(Sender: TObject);
begin
  rulesPanel.Visible:=false;
end;

procedure TwarForm.closeRulesHover(Sender: TObject);
begin
  closeRulesButton.Font.Style:=[fsBold];
end;

procedure TwarForm.closeRulesLeave(Sender: TObject);
begin
  closeRulesButton.Font.Style:=[];
end;

procedure TwarForm.lbl1RoundHover(Sender: TObject);
begin
  lbl1Round.Font.Style:= [fsBold];
end;

procedure TwarForm.lbl1RoundLeave(Sender: TObject);
begin
  lbl1Round.Font.Style:= [];
end;

procedure TwarForm.lblNumberP1Click(Sender: TObject);
var
  Index:Integer;
begin
  if lst1.Visible=false then
  begin
    for Index := 0 to Player1.PlayerDeck.Count-1 do
      begin
        lst1.Items.Add(Player1.PlayerDeck.Items[index].Name);
      end;
    lst1.Visible:=True;
  end
  else
  begin
    lst1.Visible:=False;
    lst1.Clear;
  end;
end;

procedure TwarForm.lblNumberP1Hover(Sender: TObject);
begin
  lblNumberP1.Font.Style:= [fsBold];
end;

procedure TwarForm.lblNumberP1Leave(Sender: TObject);
begin
  lblNumberP1.Font.Style:=[];
end;

procedure TwarForm.lblNumberP2Click(Sender: TObject);
var
  Index:Integer;
begin
  if lst2.Visible=false then
  begin
    for Index := 0 to Player2.PlayerDeck.Count-1 do
      begin
      lst2.Items.Add(Player2.PlayerDeck.Items[index].Name);
      end;
    lst2.Visible:=True;
  end
  else
  begin
    lst2.Visible:=False;
    lst2.Clear;
  end;
end;

procedure TwarForm.lblNumberP2Hover(Sender: TObject);
begin
  lblNumberP2.Font.Style:=[fsBold];
end;

procedure TwarForm.lblNumberP2Leave(Sender: TObject);
begin
  lblNumberP2.Font.Style:=[];
end;

procedure TwarForm.lblPlayXRoundsHover(Sender: TObject);
begin
  lblPlayXRounds.Font.Style:= [fsBold];
end;

procedure TwarForm.lblPlayXRoundsLeave(Sender: TObject);
begin
  lblPlayXRounds.Font.Style:= [];
end;

procedure TwarForm.lblToTheEndHover(Sender: TObject);
begin
  lblToTheEnd.Font.Style:= [fsBold];
end;

procedure TwarForm.lblToTheEndLeave(Sender: TObject);
begin
  lblToTheEnd.Font.Style:= [];
end;

procedure TwarForm.newButtonHover(Sender: TObject);
begin
  newButton.Font.Style:=[fsBold];
end;

procedure TwarForm.newButtonLeave(Sender: TObject);
begin
  newButton.Font.Style:=[];
end;

procedure TwarForm.helpButtonClick(Sender: TObject);
begin
  rulesPanel.Visible:=true;
end;

procedure TwarForm.helpButtonHover(Sender: TObject);
begin
  helpButton.Font.Style:=[fsbold];
end;

procedure TwarForm.helpButtonLeave(Sender: TObject);
begin
  helpButton.Font.Style:=[];
end;

procedure TwarForm.Label1Click(Sender: TObject);
begin

end;

// The returned integer has the following value :
//
//   > 0 : (positive) Item1 is less than Item2
//     0 : Item1 is equal to Item2
//   < 0 : (negative) Item1 is greater than Item2
function compareByOriginalIndex(Item1, Item2 : Pointer) : Integer;
var
  card1, card2 : TCard;
begin
  // We start by viewing the object pointers as TCard objects
  card1 := TCard(Item1);
  card2 := TCard(Item2);

  // Now compare OriginalIndex
  Result := card1.OriginalIndex - card2.OriginalIndex;
end;


// The returned integer has the following value :
//
//   > 0 : (positive) Item1 is less than Item2
//     0 : Item1 is equal to Item2
//   < 0 : (negative) Item1 is greater than Item2
function compareByRandomIndex(Item1 : TCard ; Item2 : TCard) : Integer;
begin
  // We start by viewing the object pointers as TCard objects
  Result := TCard(Item1).RandomIndex - TCard(Item2).RandomIndex;
end;

procedure Shuffle();
var
    I,Index,minIndex:integer;
    MinCard:TCard;
begin
    ////deck.Sort(TDelegatedComparer<TCard>.Create(function (const Left,Right : TCard):Integer begin
    ////Result:=Left.randomIndex-Right.randomIndex;
    ////end));
    for I:=Game.gameDeck.Count-2 downto 0 do
        begin
        minIndex:=I+1;
        for Index:=0 to I do
          begin
              if Game.gameDeck.Items[Index].randomIndex<Game.gameDeck.Items[minIndex].randomIndex then minIndex:=Index;
          end;
        MinCard := Game.gameDeck.Items[minIndex];
        Game.gameDeck.Add(MinCard);
        Game.gameDeck.Delete(minIndex);
        end;
    MinCard := Game.gameDeck.Items[minIndex];
    Game.gameDeck.Add(MinCard);
    Game.gameDeck.Delete(minIndex);
end;

procedure CreatePlayers();
var
  player1Name, player2Name : string;
  player1Score,player2Score:Integer;
begin
  player1Name:=inputbox('Players','Player 1: ', '');
  player2Name:=inputbox('Players', 'Player 2: ','');

  warForm.player1Label.Caption := player1Name;
  warForm.player2Label.Caption := player2Name;

  player1Deck:=TList<TCard>.Create;
  player2Deck:=TList<TCard>.Create;

  Player1:=TPlayer.Create(player1Name, player1Deck, 0,0);
  Player2:=TPlayer.Create(player2Name, player2Deck, 0,0);
  warForm.scoreP1.Caption:=IntToStr(Player1.Score);
  warForm.scoreP2.Caption:=IntToStr(Player2.Score);
end;

procedure FinalSort();
var
  Index:Integer;
begin
      for Index := 0 to Game.GameDeck.Count-1 do

      begin
        Game.GameDeck.Items[Index].RandomIndex:=Game.GameDeck.Items[Index].OriginalIndex;
      end;
    Shuffle();
end;

procedure Dealing();
var
  Index:Integer;
begin
  for Index := 1 to 26 do
  begin
    Player1.PlayerDeck.Add(Game.GameDeck.Items[0]);
    Game.GameDeck.Delete(0);
    Player2.PlayerDeck.Add(Game.GameDeck.Items[0]);
    Game.GameDeck.Delete(0);
  end;
  warForm.lblNumberP1.Caption:=IntToStr(Player1.PlayerDeck.Count);
  warForm.lblNumberP2.Caption:=IntToStr(Player2.PlayerDeck.Count);
end;

procedure createDeck();
var
	suitIndex,valueIndex : integer;
  card: TCard;

  name,picfile,shortName : string;
  value,originalIndex,randomIndex,order : integer;

begin
  Randomize;
  deck:=TList<TCard>.Create;
  order:=0;
  for suitIndex:=1 to 4 do
  begin
    for valueIndex := 1 to 13 do
    begin
      name:=cardsValue[valueIndex]+' of '+suits[suitIndex];
      value:=valueIndex ;
      shortName:=IntToStr(valueIndex)+suits[suitIndex][1];
      if (valueIndex=1) then
        shortName:='A'+suits[suitIndex][1]
      else if (valueIndex=11) then
        shortName:='J'+suits[suitIndex][1]
      else if (valueIndex=12) then
        shortName:='Q'+suits[suitIndex][1]
      else if (valueIndex=13) then
        shortName:='K'+suits[suitIndex][1];

      originalIndex := order;
      randomIndex := 1+Random(999);
      picfile:='assets/'+shortName+'.png';
      card:=TCard.Create(name,value, originalIndex,randomIndex ,picfile,shortName);
      deck.Add(card);
      order:=order+1;
    end;
  end;
  Counter:=0;
  pool:=TList<TCard>.Create;
  Game:=TGame.Create(Counter , Player1,Player2,deck,pool);

end;

procedure DeckFromWinnerToGameDeck();
var
  Index:Integer;
begin
  Game.GameDeck.Clear();
  for Index := 0 to Player1.PlayerDeck.Count-1 do
    Game.GameDeck.Add(Player1.PlayerDeck.Items[Index]);
  for Index := 0 to Player2.PlayerDeck.Count-1 do
    Game.GameDeck.Add(Player2.PlayerDeck.Items[index]);
  Player1.PlayerDeck.Clear;
  Player2.PlayerDeck.Clear;
end;

procedure ReShuffle();
var
  Index:Integer;
begin
  Randomize;
  for Index := 0 to Game.GameDeck.Count-1 do
  begin
    Game.GameDeck.Items[Index].RandomIndex:= 1+Random(999);
  end;
  Shuffle();
end;

procedure TwarForm.newButtonClick(Sender: TObject);
begin
  //new game
  DeckFromWinnerToGameDeck();
  ReShuffle();
  Player1.PlayerDeck.Clear;
  Player2.PlayerDeck.Clear;
  Dealing();
  Game.Counter:=0;
  warForm.lblcounter.Caption:=IntToStr(Game.counter);
  Player1.Score:=0;
  Player2.Score:=0;
  warForm.lblNumberP1.Caption:=IntToStr(Player1.PlayerDeck.Count);
  warForm.lblNumberP2.Caption:=IntToStr(Player2.PlayerDeck.Count);
  warForm.scoreP1.Caption:= IntToStr(Player1.Score);
  warForm.scoreP2.Caption:= IntToStr(Player2.Score);
  warForm.Player1TotalScoreLabel.Caption:= IntToStr(Player1.TotalScore);
  warForm.Player2TotalScoreLabel.Caption:= IntToStr(Player2.TotalScore);
  newButton.Visible:=false;
  warForm.player1CurrentCard.Picture.LoadFromFile('assets/backCard.png');
  warForm.player2CurrentCard.Picture.LoadFromFile('assets/backCard.png');
  warForm.lblWinner.Caption:='';
end;

procedure TwarForm.FormCreate(Sender: TObject);
begin
  //cr�ation des joueurs
  CreatePlayers();

//initialisation du deck de 52 cartes
  createDeck();

// m�lange du deck
  Shuffle();
  //Game.GameDeck.Sort();

//distribution des cartes aux joueurs
  Dealing();

  warForm.player1CurrentCard.Picture.LoadFromFile('assets/backCard.png');
  warForm.player2CurrentCard.Picture.LoadFromFile('assets/backCard.png');

  warForm.Player1TotalScoreLabel.Caption:= IntToStr(Player1.TotalScore);
  warForm.Player2TotalScoreLabel.Caption:= IntToStr(Player2.TotalScore);

end;

procedure DisplayWinner();
begin
          if Player1.PlayerDeck.Count=0 then
          begin
             warForm.lblWinner.Caption:=UpperCase(Player2.Name+' wins!');
             Player2.FTotalScore:=Player2.FTotalScore+1;
             warForm.Player2TotalScoreLabel.Caption:= IntToStr(Player2.TotalScore);
          end;
          if Player2.PlayerDeck.Count=0 then
          begin
             warForm.lblWinner.Caption:=UpperCase(Player1.Name+' wins!');
             Player1.FTotalScore:=Player1.FTotalScore+1;
             warForm.Player1TotalScoreLabel.Caption:= IntToStr(Player1.TotalScore);
          end;

end;

procedure play1Turn();
var
  cardP1,cardP2 : TCard;
begin
  Game.counter:=Game.counter+1;
  warForm.lblcounter.Caption:=IntToStr(Game.counter);
  cardP1:=Player1.PlayerDeck.Items[0];
  cardP2:=Player2.PlayerDeck.Items[0];
  warForm.player1CurrentCard.Picture.LoadFromFile(cardP1.FPicFile);
  warForm.player2CurrentCard.Picture.LoadFromFile(cardP2.FPicFile);
  Player1.PlayerDeck.Remove(cardP1);
  Player2.PlayerDeck.Remove(cardP2);

  if cardP1.Value<cardP2.Value then
    begin
      Player2.PlayerDeck.Add(cardP2);
      Player2.PlayerDeck.Add(cardP1);
      Player2.Score:=Player2.Score+1;
    end
  else if cardP2.Value<cardP1.Value then
    begin
      Player1.PlayerDeck.Add(cardP1);
      Player1.PlayerDeck.Add(cardP2);
      Player1.Score:=Player1.Score+1;
    end
  else
    begin
    pool.Add(cardP1);
    pool.Add(cardP2);
      while (cardP1.Value=cardP2.Value) AND (Player1.PlayerDeck.Count>1) AND (Player2.PlayerDeck.Count>1)  do
      begin
        pool.Add(Player1.PlayerDeck.Items[0]);
        Player1.PlayerDeck.Delete(0);
        pool.Add(Player2.PlayerDeck.Items[0]);
        Player2.PlayerDeck.Delete(0);
        cardP1:=Player1.PlayerDeck.Items[0];
        cardP2:=Player2.PlayerDeck.Items[0];
        warForm.player1CurrentCard.Picture.LoadFromFile(cardP1.FPicFile);
        warForm.player2CurrentCard.Picture.LoadFromFile(cardP2.FPicFile);
        pool.Add(cardP1);
        pool.Add(cardP2);
        Player1.PlayerDeck.Remove(cardP1);
        Player2.PlayerDeck.Remove(cardP2);
      end;

      if (Player1.PlayerDeck.Count=1) or (Player2.PlayerDeck.Count=1) then
        begin
          pool.Add(Player1.PlayerDeck.Items[0]);
          Player1.PlayerDeck.Delete(0);
          pool.Add(Player2.PlayerDeck.Items[0]);
          Player2.PlayerDeck.Delete(0);
        end;


      while pool.Count>0 do
      begin
        if cardP1.Value>cardP2.Value then
          begin
            Player1.PlayerDeck.Add(pool.Items[0]);
            pool.Delete(0);
          end
        else
          begin
            Player2.PlayerDeck.Add(pool.Items[0]);
            pool.Delete(0);
          end;

        if (pool.Count=1) then
          begin
            if (cardP1.Value>cardP2.Value) then
              Player1.Score:=Player1.Score+1
            else if (cardP2.Value>cardP1.Value) then
              Player2.Score:=Player2.Score+1;
          end;
      end;
    end;

  warForm.lblNumberP1.Caption:=IntToStr(Player1.PlayerDeck.Count);
  warForm.lblNumberP2.Caption:=IntToStr(Player2.PlayerDeck.Count);
  warForm.scoreP1.Caption:=IntToStr(Player1.Score);
  warForm.scoreP2.Caption:=IntToStr(Player2.Score);
  DisplayWinner();
end;


procedure TwarForm.lbl1RoundClick(Sender: TObject);
begin
  if Player1.PlayerDeck.Count*Player2.PlayerDeck.Count=0 then
    begin
      Application.MessageBox('You have reached the end of the game.', 'Game Over', 0);
      newButton.Visible:=true;
    end
  else if Player1.PlayerDeck.Count*Player2.PlayerDeck.Count>0 then
    play1Turn()
end;

procedure TwarForm.lblPlayXRoundsClick(Sender: TObject);
var
  I:Integer;
begin
  for I := 1 to 50 do
      begin
        if Player1.PlayerDeck.Count*Player2.PlayerDeck.Count>0 then
          play1Turn()
        else if Player1.PlayerDeck.Count*Player2.PlayerDeck.Count=0 then
          begin
            Application.MessageBox('You have reached the end of the game.', 'Game Over', 0);
            newButton.Visible:=true;
            break;
          end;
      end;
end;

procedure TwarForm.lblToTheEndClick(Sender: TObject);
begin
  if Player1.PlayerDeck.Count*Player2.PlayerDeck.Count=0 then
      Application.MessageBox('You have reached the end of the game.', 'Game Over', 0)
  else
    begin
      while Player1.PlayerDeck.Count*Player2.PlayerDeck.Count>0 do
        begin
          if Player1.PlayerDeck.Count*Player2.PlayerDeck.Count>0 then
            play1Turn();
        end;
    end;

  newButton.Visible:=true;
end;

procedure TwarForm.closeButtonClick(Sender: TObject);
begin
  if (Application.MessageBox('Are you sure that you want to close the application?', 'Warning - Closing the application', MB_OKCANCEL Or MB_ICONEXCLAMATION))=IDOK then
  begin
    //Reconstitution du deck
    DeckFromWinnerToGameDeck();
    //Tri du deck
    FinalSort();
    //Destruction de tous les objets
    Game.Free;
    Application.MessageBox('The deck has been sorted for your next game.', 'Deck sorted', MB_OK Or MB_ICONASTERISK);
    warForm.Close;
  end;
end;

end.
